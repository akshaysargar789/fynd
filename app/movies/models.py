from django.db import models

# Create your models here.
class Genre(models.Model):
    name = models.CharField(max_length=100, null=True)

    def __str__(self):
        return self.name

class Movie(models.Model):
    name = models.CharField(max_length=100, null=True)
    director = models.CharField(max_length=100, null=True)
    imdb_score = models.FloatField(null=True)
    popularity = models.FloatField(null=True)
    genre = models.ManyToManyField(Genre)

    def __str__(self):
        return self.name

