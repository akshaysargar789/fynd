from django.shortcuts import render, redirect
from .models import Movie, Genre
from django.contrib.auth.decorators import login_required

# Create your views here.
@login_required(login_url="login")
def dashboard(request):

    movies = Movie.objects.all()    
    genres = Genre.objects.all()
    return render(request, 'dashboard.html', {'movies': movies, 'genres': genres})

def addMovie(request):
    if request.method == 'POST':

        name = request.POST.get('name')
        director = request.POST.get('director')
        imdb_score = request.POST.get('imdb_score')
        popularity = request.POST.get('popularity')
        genre = request.POST.get('genre')
        

        movies = Movie.objects.create(name=name, director=director, imdb_score=imdb_score, popularity=popularity)
        movies.genre.add(genre)

        return redirect('dashboard')

def editMovie(request):

    movies = Movie.objects.filter(id=request.movie_id).update()    
    return render(request, 'dashboard.html', {'movies': movies})

def deleteMovie(request, movie_id):

    movies = Movie.objects.filter(id=movie_id).delete()    
    return redirect('dashboard')
