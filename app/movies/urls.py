from django.urls import path

from . import views

urlpatterns = [
    path('dashboard', views.dashboard, name='dashboard'),
    path('add', views.addMovie, name='add'),
    path('edit/<int:movie_id>', views.editMovie, name='edit'),
    path('delete/<int:movie_id>', views.deleteMovie, name='delete')
]