# Generated by Django 3.0.7 on 2020-07-10 18:27

from django.db import migrations, models


class Migration(migrations.Migration):

    initial = True

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='Genre',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('name', models.CharField(max_length=100, null=True)),
            ],
        ),
        migrations.CreateModel(
            name='Movie',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('name', models.CharField(max_length=100, null=True)),
                ('director', models.CharField(max_length=100, null=True)),
                ('imdb_score', models.FloatField(null=True)),
                ('popularity', models.FloatField(null=True)),
                ('genre', models.ManyToManyField(to='movies.Genre')),
            ],
        ),
    ]
