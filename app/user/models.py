from django.db import models

# Create your models here.

class User(models.Model):
    first_name = models.CharField(max_length=100, null=True)
    last_name = models.CharField(max_length=100, null=True)
    password = models.CharField(null=True)
    email = models.EmailField(null=True)
    username = models.CharField(max_length=100, null=True)